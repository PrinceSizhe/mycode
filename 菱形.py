#!/usr/bin/env python3
n = int(input())
for i in range((n + 1) // 2):
    print(('*' * (2 * i + 1)).center(n).rstrip())
for i in reversed(range((n - 1) // 2)):
    print(('*' * (2 * i + 1)).center(n).rstrip())
